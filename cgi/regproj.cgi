#!/usr/bin/perl
# (c) Petr Baudis <pasky@suse.cz>
# GPLv2

use strict;
use warnings;

use lib ".";
use Girocco::CGI;
use Girocco::Config;
use Girocco::Project;
use Girocco::Util;

my $gcgi = Girocco::CGI->new('Project Registration');
my $cgi = $gcgi->cgi;

my $name = $cgi->param('name');
$name ||= '';

my $fork = $cgi->param('fork');
if ($fork) {
	$fork =~ s/\.git$//;
	$name = "$fork/$name";
}

if ($cgi->param('mode')) {
	# submitted, let's see
	# FIXME: racy, do a lock
	Girocco::Project::valid_name($name)
		and Girocco::Project::does_exist($name)
		and $gcgi->err("Project with the name '$name' already exists.");
	$name =~ /\.git$/
		and $gcgi->err("Project name should not end with <tt>.git</tt> - I'll add that automagically.");

	if ($cgi->param('mail') !~ /^(?:the )?sun$/i) {
		print "<p>Sorry, invalid captcha check.</p>";
		exit;
	}

	my $mirror = $cgi->param('mode') eq 'mirror';

	if ($mirror and $Girocco::Config::mirror_sources and not $cgi->param('url')) {
		my $src = $cgi->param('source'); $src ||= '';
		my $source; $src and $source = (grep { $_->{label} eq $src } @$Girocco::Config::mirror_sources)[0];
		$source or $gcgi->err("Invalid or no mirror source $src specified");

		my $n = $source->{label};
		my $u = $source->{url};
		if ($source->{inputs}) {
			for my $i (0..$#{$source->{inputs}}) {
				my $v = $cgi->param($n.'_i'.$i);
				unless ($v) {
					$gcgi->err("Source specifier '".$source->{inputs}->[$i]->{label}."' not filled.");
					next;
				}
				my $ii = $i + 1;
				$u =~ s/%$ii/$v/g;
			}
		} else {
			$u = $cgi->param($n.'_url');
			$u or $gcgi->err("Source URL not specified");
		}
		$cgi->param('url', $u);
	}

	my $proj = Girocco::Project->ghost($name, $mirror);
	if ($proj->cgi_fill($gcgi)) {
		if ($mirror) {
			unless ($Girocco::Config::mirror) {
				$gcgi->err("Mirroring mode is not enabled at this site.");
				exit;
			}
			$proj->premirror;
			print "<p>Please <a href=\"mirrorproj.cgi?name=$name\">pass onwards</a>.</p>\n";
			print "<script language=\"javascript\">document.location='mirrorproj.cgi?name=$name'</script>\n";

		} else {
			unless ($Girocco::Config::push) {
				$gcgi->err("Push mode is not enabled at this site.");
				exit;
			}
			$proj->conjure;
			print <<EOT;
<p>
Project <a href="$Girocco::Config::gitweburl/$name.git">$name</a> successfuly set up.</p>
EOT
			print "<p>The push URL for the project is <tt>$Girocco::Config::pushurl/$name.git</tt>.</p>";
			print "<p>The read-only URL for the project is <tt>" .
				join("/$name.git</tt>, <tt>", $Girocco::Config::gitpullurl, $Girocco::Config::httppullurl) .
				"/$name.git</tt>.</p>" if $Girocco::Config::gitpullurl or $Girocco::Config::httppullurl;
			my $regnotice = '';
			if ($Girocco::Config::manage_users) {
				$regnotice = <<EOT;
Everyone who wants to push must <a href="reguser.cgi">register himself as a user</a> first.
(One user can have push access to multiple projects and multiple users can have push access to one project.)
EOT
			}
			print <<EOT;
<p>You can <a href="editproj.cgi?name=$name">assign users</a> now
- don't forget to assign yourself as a user as well if you want to push!
$regnotice
</p>
<p>Note that you cannot clone an empty repository since it contains no branches; you need to make the first push from an existing repository.
To import a new project, the procedure is roughly as follows:
<pre>
  \$ git init
  \$ git add
  \$ git commit
  \$ git remote add origin $Girocco::Config::pushurl/$name.git
  \$ git push --all origin
</pre>
</p>
<p>You may experience permission problems if you try to push right now.
If so, that should get fixed automagically in few minutes, please be patient.</p>
<p>Enjoy yourself, and have a lot of fun!</p>
EOT
		}
		exit;
	}
}

my $mirror_mode = {
	name => 'mirror',
	desc => 'our dedicated git monkeys will check another repository at a given URL every hour and mirror any new updates',
	pwpurp => 'mirroring URL'
};
my $push_mode = {
	name => 'push',
	desc => 'registered users with appropriate permissions will be able to push to the repository',
	pwpurp => 'list of users allowed to push'
};

my $me = $Girocco::Config::mirror ? $mirror_mode : undef;
my $pe = $Girocco::Config::push ? $push_mode : undef;
if ($me and $pe) {
	print <<EOT;
<p>At this site, you can host a project in one of two modes: $me->{name} mode and $pe->{name} mode.
In the <b>$me->{name} mode</b>, $me->{desc}.
In the <b>$pe->{name} mode</b>, $pe->{desc}.
You currently cannot switch freely between those two modes;
if you want to switch from mirroring to push mode, just delete and recreate
the project. If you want to switch the other way, please contact the administrator.</p>
EOT
} else {
	my $mode = $me ? $me : $pe;
	print "<p>This site will host your project in a <b>$mode->{name} mode</b>: $mode->{desc}.</p>\n";
}

my @pwpurp = ();
push @pwpurp, $me->{pwpurp} if $me;
push @pwpurp, $pe->{pwpurp} if $pe;
my $pwpurp = join(', ', @pwpurp);

if ($Girocco::Config::project_passwords) {
	print <<EOT;
<p>You will need the admin password to adjust the project settings later
($pwpurp, project description, ...).</p>
EOT
}

unless ($name =~ m#/#) {
	print <<EOT;
<p>Note that if your project is a <strong>fork of an existing project</strong>
(this does not mean anything socially bad), please instead go to the project's
gitweb page and click the 'fork' link in the top bar. This way, all of us
will save bandwidth and more importantly, your project will be properly categorized.
EOT
	$me and print <<EOT;
If your project is a fork but the existing project is not registered here yet, please
consider registering it first; you do not have to be involved in the project
in order to register it here as a mirror.</p>
EOT
} else {
	my $xname = $name; $xname =~ s#/$#.git#; #
	my ($pushnote1, $pushnote2);
	if ($pe) {
		$pushnote1 = " and you will need to push only the data <em>you</em> created, not the whole project";
		$pushnote2 = <<EOT;
(That will be done automagically, you do not need to specify any extra arguments during the push.
EOT
	}
	print <<EOT;
<p>Great, your project will be created as a subproject of the '$xname' project.
This means that it will be properly categorized$pushnote1. $pushnote2</p>
EOT
}

my $modechooser;
my $mirrorentry = '';
if ($me) {
	$mirrorentry = '<tr id="mirror_url"><td class="formlabel">Mirror source:</td><td>';
	if (!$Girocco::Config::mirror_sources) {
		$mirrorentry .= '<input type="text" name="url" />';
	} else {
		$mirrorentry .= "<table>"."\n";
		my $checked = ' checked=checked';
		foreach my $source (@$Girocco::Config::mirror_sources) {
			my $n = $source->{label};
			$mirrorentry .= '<tr><td class="formlabel">';
			$mirrorentry .= '<p><input type="radio" name="source" value="'.$n.'" '.$checked.' />';
			$mirrorentry .= $n;
			$mirrorentry .= '</p></td><td>';
			$checked = '';
			if ($source->{desc}) {
				$mirrorentry .= '<p>';
				$source->{link} and $mirrorentry .= '<a href="'.$source->{link}.'">';
				$mirrorentry .= $source->{desc};
				$source->{link} and $mirrorentry .= '</a>';
				$mirrorentry .= '</p>';
			}
			if (!$source->{inputs}) {
				$mirrorentry .= '<p>URL: <input type="text" name="'.$n.'_url" /></p>';
			} else {
				$mirrorentry .= '<p>';
				my $i = 0;
				foreach my $input (@{$source->{inputs}}) {
					$mirrorentry .= $input->{label};
					my ($l, $v) = ($n.'_i'.$i, '');
					if ($cgi->param($l)) {
						$v = ' value="'.html_esc($cgi->param($l)).'"';
					}
					$mirrorentry .= ' <input type="text" name="'.$l.'"'.$v.' />';
					$mirrorentry .= $input->{suffix} if $input->{suffix};
					$mirrorentry .= '&nbsp; &nbsp;';
				} continue { $i++; }
				$mirrorentry .= '</p>';
			}
			$mirrorentry .= '</td></tr>'."\n";
		}
		$mirrorentry .= "</table>";
	}
	$mirrorentry .= '</td></tr>';
}
if ($me and $pe) {
	$modechooser = <<EOT;
<tr><td class="formlabel">Hosting mode:</td><td><p>
<input type="radio" name="mode" value="mirror" id="mirror_radio" checked="checked" />Mirror mode<br />
<input type="radio" name="mode" value="push" id="push_radio" />Push mode
</p></td></tr>
EOT
} else {
	$modechooser = '<input type="hidden" name="mode" value="'.($me ? $me->{name} : $pe->{name}).'" />';
}

my $forkentry = '';
if ($name =~ m#/#) {
	$name =~ s#^(.*)/##;
	$forkentry = '<input type="hidden" name="fork" value="'.$1.'" />'.$1.'/'
}

print <<EOT;
$Girocco::Config::legalese
<form method="post">
<table class="form">
<tr><td class="formlabel">Project name:</td><td>$forkentry<input type="text" name="name" value="$name" />.git</td></tr>
EOT
if ($Girocco::Config::project_passwords) {
	print <<EOT;
<tr><td class="formlabel">Admin password (twice):</td><td><input type="password" name="pwd" /><br /><input type="password" name="pwd2" /></td></tr>
EOT
}
if ($Girocco::Config::project_owners eq 'email') {
	print <<EOT;
<tr><td class="formlabel">E-mail contact:</td><td><input type="text" name="email" /></td></tr>
EOT
}
print $modechooser;
print $mirrorentry;

$gcgi->print_form_fields($Girocco::Project::metadata_fields, undef, @Girocco::Config::project_fields);

print <<EOT;
EOT

print <<EOT;
<tr><td class="formlabel">Anti-captcha - please<br />enter name of our nearest star:</td><td><input type="text" name="mail" /></td></tr>
<tr><td></td><td><input type="submit" name="y0" value="Register" /></td></tr>
</table>
</form>
EOT
