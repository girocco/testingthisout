#!/usr/bin/perl
# (c) Petr Baudis <pasky@suse.cz>
# GPLv2

use strict;
use warnings;

use lib ".";
use Girocco::CGI;
use Girocco::Config;
use Girocco::Project;
use Girocco::Util;

my $gcgi = Girocco::CGI->new('Project Settings');
my $cgi = $gcgi->cgi;

my $name = $cgi->param('name');
$name =~ s#\.git$## if $name; #

unless (defined $name) {
	print "<p>I need the project name as an argument now.</p>\n";
	exit;
}

if (!Girocco::Project::valid_name($name)) {
	print "<p>Invalid project name. Go away, sorcerer.</p>\n";
	exit;
}

if (!Girocco::Project::does_exist($name)) {
	print "<p>Sorry but the project $name does not exist. Now, how did you <em>get</em> here?!</p>\n";
	exit;
}

my $proj = Girocco::Project->load($name);
$proj or die "not found project $name, that's really weird!";

if ($cgi->param('y0')) {
	# submitted, let's see
	if ($proj->cgi_fill($gcgi) and $proj->authenticate($gcgi) and $proj->update) {
		print "<p>Project successfuly updated.</p>\n";
		if ($proj->{clone_failed}) {
			print "<p>Please <a href=\"mirrorproj.cgi?name=$name\">pass onwards</a>.</p>\n";
			print "<script language=\"javascript\">document.location='mirrorproj.cgi?name=$name'</script>\n";
			exit;
		}
	}
}

# $proj may be insane now but that's actually good for us since we'll let the
# user fix the invalid values she entered
my %h = $proj->form_defaults;

print <<EOT;
<p>Here you can adjust the settings of project $h{name}. Go wild.
EOT
if ($proj->{mirror}) {
	print <<EOT;
Since this is a mirrored project, you can opt to remove it from the site as well.
Just <a href="delproj.cgi?name=$h{name}">delete it</a>.</p>
EOT
} else {
	print <<EOT;
Though you can currently enable access only for a single user at a time
so perhaps you will need to click a lot. Sorry! (Patches welcome.)</p>
EOT
}

my $button_label = $proj->{clone_failed} ? 'Restart Mirroring' : 'Update';

print <<EOT;
<form method="post">
<table class="form">
<tr><td class="formlabel">Project name:</td><td><a href="$Girocco::Config::gitweburl/$h{name}.git">$h{name}</a>.git
	<input type="hidden" name="name" value="$h{name}" /></td></tr>
EOT
if ($Girocco::Config::project_passwords) {
	print <<EOT;
<tr><td class="formlabel"><strong>Admin password:</strong></td><td>
	<input type="password" name="cpwd" /> <sup><a href="pwproj.cgi?name=$name" class="ctxaction">(forgot password?)</a></sup></td></tr>
<tr><td class="formlabel">New admin password (twice):<br />
	<em>(leave empty to keep it the same)</em></td><td>
	<input type="password" name="pwd" /><br /><input type="password" name="pwd2" /><br />
	</td></tr>
EOT
}
if ($Girocco::Config::project_owners eq 'email') {
	print <<EOT;
<tr><td class="formlabel">E-mail contact:</td><td><input type="text" name="email" value="$h{email}" /></td></tr>
EOT
}

if ($proj->{mirror}) {
	print "<tr><td class=\"formlabel\">Repository URL:</td><td><input type=\"text\" name=\"url\" value=\"$h{url}\" /></td></tr>\n";
} else {
	print <<EOT;
<tr><td class="formlabel">Users:</td><td>
<ul>
EOT
	$Girocco::Config::manage_users and print "<p>Only <a href=\"reguser.cgi\">registered users</a> can push.</p>";
	if ($Girocco::Config::mob and not grep { $_ eq $Girocco::Config::mob } @{$h{users}}) {
		print "<p><em>(Please consider adding the <tt>$Girocco::Config::mob</tt> user.\n";
		print "<sup><a href=\"$Girocco::Config::htmlurl/mob.html\">(learn more)</a></sup>)\n";
		print "</em></p>\n";
	}
	foreach my $user (@{$h{users}}) {
		print "<li><input type=\"checkbox\" name=\"user\" value=\"$user\" checked=\"1\" /> $user</li>\n";
	}
	print <<EOT;
<li>Add user: <input type="text" name="user" /></li>
</ul>
</td></tr>
EOT
}

print '<tr><td class="formlabel">Default branch:</td><td><select size="1" name="HEAD">';
for ($proj->get_heads) {
	my $selected = $proj->{HEAD} eq $_ ? ' selected="selected"' : '';
	print "<option$selected>".Girocco::CGI::html_esc($_)."</option>";
}
print '</select></td></tr>
';

$gcgi->print_form_fields($Girocco::Project::metadata_fields, \%h, @Girocco::Config::project_fields);

print <<EOT;
<tr><td></td><td><input type="submit" name="y0" value="$button_label" /></td></tr>
</table>
</form>
EOT
