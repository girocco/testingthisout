#!/usr/bin/perl
# (c) Petr Baudis <pasky@suse.cz>
# GPLv2

use strict;
use warnings;

use lib ".";
use Girocco::CGI;
use Girocco::Config;
use Girocco::Project;
use Girocco::Util;


sub genpwd {
	# FLUFFY!
	substr(crypt(rand, rand), 2);
}


my $gcgi = Girocco::CGI->new('Forgotten Project Password');
my $cgi = $gcgi->cgi;

my $name = $cgi->param('name');

unless (defined $name) {
	print "<p>I need the project name as an argument.</p>\n";
	exit;
}

if (!Girocco::Project::valid_name($name)) {
	print "<p>Invalid project name. Go away, sorcerer.</p>\n";
	exit;
}

if (!Girocco::Project::does_exist($name)) {
	print "<p>Sorry but this project does not exist. Now, how did you <em>get</em> here?!</p>\n";
	exit;
}

my $proj = Girocco::Project->load($name);
$proj or die "not found project $name, that's really weird!";

my $mail = $proj->{email};

if ($cgi->param('y0')) {
	# submitted

	my $newpwd = genpwd();

	open (M, '|-', 'mail', '-s', "[$Girocco::Config::name] New password for project $name", $mail) or die "Cannot spawn mail: $!";
	print M <<EOT;
Hello,

somebody asked for the password for project $name to be reset. Since you are
the project admin, you get to know the new password:

	$newpwd

In case you did not request the password to be reset, we apologize. Nevertheless,
you have to use the reset password now (possibly to change it back).

Quick-link to the edit project page:

	$Girocco::Config::webadmurl/editproj.cgi?name=$name

Have fun!
EOT
	close M or die "mail $mail for $name died? $!";

	$proj->update_password($newpwd);

	print "<p>Project password has been reset. Have a nice day.</p>\n";
	exit;
}

print <<EOT;
<p>You are trying to make me reset password for project $name. I will send the new
password to the project admin &lt;$mail&gt;.</p>
<form method="post">
<input type="hidden" name="name" value="$name" />
<p><input type="submit" name="y0" value="Reset Password" /></p>
</form>
EOT

