#!/usr/bin/perl
# (c) Petr Baudis <pasky@suse.cz>
# GPLv2

use strict;
use warnings;

use lib ".";
use Girocco::CGI;
use Girocco::Config;
use Girocco::Project;
use Girocco::Util;

my $gcgi = Girocco::CGI->new('Project Mirroring');
my $cgi = $gcgi->cgi;

my $name = $cgi->param('name');
$name =~ s#\.git$## if $name; #

unless (defined $name) {
	print "<p>I need the project name as an argument now.</p>\n";
	exit;
}

if (!Girocco::Project::valid_name($name)) {
	print "<p>Invalid project name. Go away, sorcerer.</p>\n";
	exit;
}

if (!Girocco::Project::does_exist($name)) {
	print "<p>Sorry but the project $name does not exist. Now, how did you <em>get</em> here?!</p>\n";
	exit;
}

my $proj = Girocco::Project->load($name);
$proj or die "not found project $name, that's really weird!";

if (!$proj->{mirror} or !$proj->{clone_in_progress}) {
	print "<p>This project is not a mirror to be cloned.</p>\n";
	exit;
}


$| = 1;

if (!$proj->{clone_logged} or $proj->{clone_failed}) {
	# Kick off the clone since it is not running yet
	print "<p>Initiated mirroring of ".$proj->{url}." to $Girocco::Config::name project ".
		"<a href=\"$Girocco::Config::gitweburl/$name.git\">$name</a>.git:</p>\n";
	$proj->clone;

} else {
	print "<p>Mirroring of ".$proj->{url}." to $Girocco::Config::name project ".
		"<a href=\"$Girocco::Config::gitweburl/$name.git\">$name</a>.git in progress:</p>\n";
}

print "<pre>\n";

open LOG, $proj->_clonelog_path() or die "clonelog: $!";
tailf: for (;;) {
	my $curpos;
	for ($curpos = tell(LOG); <LOG>; $curpos = tell(LOG)) {
		chomp;
		$_ eq '@OVER@' and last tailf;
		print "$_\n";
	}
	sleep 1;
	seek(LOG, $curpos, 0);  # seek to where we had been
}
close LOG;

print "</pre>\n";

my $proj = Girocco::Project->load($name);
$proj or die "not found project $name on second load, that's _REALLY_ weird!";

if ($proj->{clone_failed}) {
	print <<EOT;
<p><strong>Mirroring failed!</strong> Please <a href="editproj.cgi?name=$name">revisit the project settings</a>.</p>
EOT
}
