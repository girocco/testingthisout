#!/bin/sh
# The Girocco jail setup script
#
# We are designed to set up the chroot based on binaries from
# amd64 Debian lenny; some things may need slight modifications if
# being run on a different distribution.

set -e

. shlib.sh

# Verify we have all we neeed.
if ! getent group $cfg_owning_group >/dev/null; then
	echo "*** Error: You do not have $cfg_owning_group in system yet." >&2
	exit 1
fi

umask 022
mkdir -p "$cfg_chroot"
cd "$cfg_chroot"
chown root "$cfg_chroot"
chmod 755 "$cfg_chroot"

# First, setup basic directory structure
mkdir -p bin dev etc lib sbin srv/git var/run proc
rm -f usr lib64
ln -s . usr
ln -s lib lib64

# Set up basic user/group configuration; if there is any already,
# we hope it's the same numbers and users.

if [ ! -s etc/passwd ]; then
	cat >etc/passwd <<EOT
sshd:x:101:65534:priviledge separation:/var/run/sshd:/bin/false
mob::65538:65534:the mob:/:/bin/git-shell
EOT
fi

if [ ! -s etc/group ]; then
	cat >etc/group <<EOT
_repo:x:$(getent group "$cfg_owning_group" | cut -d : -f 3):
sshd:x:101:65534:priviledge separation:/var/run/sshd:/bin/false
mob::65538:65534:the mob:/:/bin/git-shell
EOT
fi

# Seed up /dev:
rm -f dev/null dev/zero dev/random dev/urandom
mknod dev/null c 1 3
mknod dev/zero c 1 5
mknod dev/random c 1 8
mknod dev/urandom c 1 9
chmod a+rw dev/null dev/zero dev/random dev/urandom

# Set up mob user:
touch var/run/mob
chown 65538 var/run/mob
chmod 0 var/run/mob

# Set up sshd configuration:
mkdir -p var/run/sshd
mkdir -p etc/sshkeys
chown $cfg_cgi_user.$cfg_owning_group etc/sshkeys
chmod g+ws etc/sshkeys
mkdir -p etc/ssh
if [ ! -s etc/ssh/sshd_config ]; then
	cat >etc/ssh/sshd_config <<EOT
Protocol	2
Port		22
UsePAM		no
X11Forwarding	no
PermitRootLogin no
UsePrivilegeSeparation yes

AuthorizedKeysFile	/etc/sshkeys/%u
StrictModes	no

# mob user:
PermitEmptyPasswords	yes
ChallengeResponseAuthentication no
PasswordAuthentication	yes
EOT
fi
if [ ! -s etc/ssh/ssh_host_dsa_key ]; then
        yes | ssh-keygen -N "" -C Girocco -t dsa -f etc/ssh/ssh_host_dsa_key
        yes | ssh-keygen -N "" -C Girocco -t rsa -f etc/ssh/ssh_host_rsa_key
fi

# Bring in basic libraries:
rm -f lib/*
# ld.so:
cp -t lib /lib/ld-linux.so.2
[ ! -d /lib64 ] || cp -t lib /lib64/ld-linux-x86-64.so.2
# libc:
cp -t lib /lib/libc.so.6 /lib/libcrypt.so.1 /lib/libutil.so.1 /lib/libnsl.so.1 /lib/libnss_compat.so.2 /lib/libresolv.so.2 /lib/libdl.so.2 /lib/libgcc_s.so.1

# Now, bring in sshd and sh.

pull_in_bin() {
	bin="$1"; dst="$2"
	cp -t "$dst" "$bin"
	# ...and all the dependencies.
	ldd "$bin" | grep -v linux-gate | grep -v linux-vdso | grep -v ld-linux | grep '=>' | awk '{print $3}' | xargs -r -- cp -u -t lib
}

pull_in_bin /bin/sh bin
pull_in_bin /bin/nc.openbsd bin
# If /sbin/sshd is already running within the chroot, we get Text file busy.
pull_in_bin /usr/sbin/sshd sbin || :

# ...and the bits of git we need.
for i in git git-index-pack git-receive-pack git-shell git-update-server-info git-upload-archive git-upload-pack git-unpack-objects; do
	pull_in_bin /usr/bin/$i bin
done

echo "--- Add to your boot scripts: mount --bind $cfg_reporoot $cfg_chroot/srv/git"
echo "--- Add to your boot scripts: mount --bind /proc $cfg_chroot/proc"
echo "--- Add to your syslog configuration: listening on socket $cfg_chroot/dev/log"
