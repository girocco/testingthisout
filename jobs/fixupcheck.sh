#!/bin/bash
#
# THIS SCRIPT IS BEING RUN UNDER ROOT!!!
#
# [repo.or.cz] You will need to manually update this file if you modify
# it in the repository.

# We want to make sure the push-access projects have the right directories
# in the right groups.

## For maximum security separation, the fixup script is configured separately
## and does not reuse Girocco::Config settings.

## Girocco::Config::reporoot
reporoot="/srv/git"
## Girocco::Config::chroot
chroot="/home/repo/j"
## Girocco::Config::mirror_user
mirror_user="repo"
## Directory with this script and fixup.sh; WARNING: COPY THEM OVER to ~root!
## Otherwise, the owner of these scripts can execute anything as root.
fixup_dir="/root/repomgr"

# Make sure we don't run twice.
if [ -s /tmp/fixuplock ] && kill -0 $(cat /tmp/fixuplock); then
	echo "Already running (stuck?) with pid $(cat /tmp/fixuplock)" >&2
	exit 1
fi
echo $$ >/tmp/fixuplock

cd "$reporoot"
cat "$chroot/etc/group" | cut -d : -f 1,3 |
	while IFS=: read proj id; do
		[ "$id" -ge 65536 ] || continue
		"$fixup_dir"/fixup.sh "$proj" "$chroot/etc/group" "$mirror_user"
	done

rm /tmp/fixuplock
