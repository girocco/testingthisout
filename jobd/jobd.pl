#!/usr/bin/perl
#
# jobd - perform Girocco maintenance jobs
#
# Run with --help for details

use strict;
use warnings;

use Getopt::Long;
use Pod::Usage;
use POSIX ":sys_wait_h";

use Girocco::Config;
use Girocco::Project;
use Girocco::User;

# Options
my $quiet;
my $progress;
my $kill_after = 900;
my $max_par = 20;
my $max_par_intensive = 1;
my $lockfile = "/tmp/jobd.lock";
my $restart_delay = 10; # not currently configurable
my $all_once;
my $one;

######### Jobs {{{1

sub update_project {
	my $job = shift;
	my $p = $job->{'project'};
	check_project_exists($job) || return;
	if (-e get_project_path($p).".nofetch") {
		job_skip($job);
		return setup_gc($job);
	}
	if (-e get_project_path($p).".clone_in_progress") {
		job_skip($job, "initial mirroring not complete yet");
		return;
	}
	if (my $ts = is_operation_uptodate($p, 'lastrefresh', $Girocco::Config::min_mirror_interval)) {
		job_skip($job, "not needed right now, last run at $ts");
		setup_gc($job);
		return;
	};
	exec_job_command($job, ["$Girocco::Config::basedir/jobd/update.sh", $p], $quiet);
}

sub gc_project {
	my $job = shift;
	my $p = $job->{'project'};
	check_project_exists($job) || return;
	if (my $ts = is_operation_uptodate($p, 'lastgc', $Girocco::Config::min_gc_interval)) {
		job_skip($job, "not needed right now, last run at $ts");
		return;
	}
	exec_job_command($job, ["$Girocco::Config::basedir/jobd/gc.sh", $p], $quiet);
}

sub setup_gc {
	my $job = shift;
	queue_job(
		project => $job->{'project'},
		type => 'gc',
		command => \&gc_project,
		intensive => 1,
	);
}

sub check_project_exists {
	my $job = shift;
	my $p = $job->{'project'};
	if (!-d get_project_path($p)) {
		job_skip($job, "non-existent project");
		return 0;
	}
	1;
}

sub get_project_path {
	"$Girocco::Config::reporoot/".shift().".git/";
}

sub is_operation_uptodate {
	my ($project, $which, $threshold) = @_;
	my $path = get_project_path($project);
	my $timestamp = `GIT_DIR="$path" $Girocco::Config::git_bin config "gitweb.$which"`;
	my $unix_ts = `date +%s -d "$timestamp"`;
	(time - $unix_ts) <= $threshold ? $timestamp : undef;
}

sub queue_one {
	my $project = shift;
	queue_job(
		project => $project,
		type => 'update',
		command => \&update_project,
		on_success => \&setup_gc,
		on_error => \&setup_gc,
	);
}

sub queue_all {
	queue_one($_) for (Girocco::Project->get_full_list());
}

######### Daemon operation {{{1

my @queue;
my @running;
my $perpetual = 1;
my $locked = 0;
my $jobs_executed;
my $jobs_skipped;
my @jobs_killed;

sub handle_softexit {
	error("Waiting for outstanding jobs to finish... ".
		"^C again to exit immediately");
	@queue = ();
	$perpetual = 0;
	$SIG{'INT'} = \&handle_exit;
}

sub handle_exit {
	error("Killing outstanding jobs...");
	$SIG{'TERM'} = 'IGNORE';
	for (@running) {
		kill 'KILL', -($_->{'pid'});
	}
	unlink $lockfile if ($locked);
	exit(0);
}

sub queue_job {
	my %opts = @_;
	$opts{'queued_at'} = time;
	$opts{'dont_run'} = 0;
	$opts{'intensive'} = 0 unless exists $opts{'intensive'};
	push @queue, \%opts;
}

sub run_job {
	my $job = shift;

	push @running, $job;
	$job->{'command'}->($job);
	if ($job->{'dont_run'}) {
		pop @running;
		$jobs_skipped++;
		return;
	}
}

sub _job_name {
	my $job = shift;
	"[".$job->{'type'}."::".$job->{'project'}."]";
}

# Only one of those per job!
sub exec_job_command {
	my ($job, $command, $err_only) = @_;

	my $pid;
	if (!defined($pid = fork)) {
		error(_job_name($job) ." Can't fork job: $!");
		$job->{'finished'} = 1;
		return;
	}
	if (!$pid) {
		open STDIN, '/dev/null' || do {
			error(_job_name($job) ."Can't read from /dev/null: $!");
			$job->{'finished'} = 1;
			return;
		};
		if ($err_only) {
			open STDOUT, '>/dev/null' || do {
				error(_job_name($job) ." Can't write to /dev/null: $!");
				$job->{'finished'} = 1;
				return;
			};
		}
		# New process group so we can keep track of all of its children
		if (!defined(POSIX::setpgid(0, 0))) {
			error(_job_name($job) ." Can't create process group: $!");
			$job->{'finished'} = 1;
			return;
		}
		# "Prevent" races
		select(undef, undef, undef, 0.1);
		exec @$command;
		# Stop perl from complaining
		exit $?;
	}
	$job->{'pid'} = $pid;
	$job->{'finished'} = 0;
	$job->{'started_at'} = time;
}

sub job_skip {
	my ($job, $msg) = @_;
	$job->{'dont_run'} = 1;
	error(_job_name($job) ." Skipping job: $msg") unless $quiet || !$msg;
}

sub reap_hanging_jobs {
	for (@running) {
		if (defined($_->{'started_at'}) && (time - $_->{'started_at'}) > $kill_after) {
			$_->{'finished'} = 1;
			kill 'KILL', -($_->{'pid'});
			print STDERR _job_name($_) ." KILLED due to timeout\n";
			push @jobs_killed, _job_name($_);
		}
	}
}

sub reap_finished_jobs {
	my $pid;
	my $finished_any = 0;
	while (1) {
		$pid = waitpid(-1, WNOHANG);
		last if $pid < 1;
		$finished_any = 1;

		my @child = grep { $_->{'pid'} && $_->{'pid'} == $pid } @running;
		if ($?) {
			# XXX- we currently don't care
		}
		if (@child && !$child[0]->{'finished'}) {
			$child[0]->{'on_success'}->($child[0]) if defined($child[0]->{'on_success'});
			$child[0]->{'finished'} = 1;
			$jobs_executed++;
		} elsif (@child) {
			$child[0]->{'on_error'}->($child[0]) if defined($child[0]->{'on_error'});
		}
	}
	@running = grep { $_->{'finished'} == 0 } @running;
	$finished_any;
}

sub have_intensive_jobs {
	grep { $_->{'intensive'} == 1 } @running;
}

sub ts {
	"[". scalar(localtime) ."] ";
}

sub run_queue {
	my $last_progress = time;
	$jobs_executed = 0;
	$jobs_skipped = 0;
	@jobs_killed = ();
	if ($progress) {
		printf STDERR ts() ."--- Processing %d queued jobs\n", scalar(@queue);
	}
	$SIG{'INT'} = \&handle_softexit;
	$SIG{'TERM'} = \&handle_exit;
	while (@queue || @running) {
		reap_hanging_jobs();
		my $proceed_immediately = reap_finished_jobs();
		# Back off if we're too busy
		if (@running >= $max_par || have_intensive_jobs() >= $max_par_intensive || !@queue) {
			sleep 1 unless $proceed_immediately;
			if ($progress && (time - $last_progress) >= 60) {
				printf STDERR ts() ."STATUS: %d queued, %d running, %d finished, %d skipped, %d killed\n", scalar(@queue), scalar(@running), $jobs_executed, $jobs_skipped, scalar(@jobs_killed);
				if (@running) {
					my @run_status;
					for (@running) {
						push @run_status, _job_name($_)." ". (time - $_->{'started_at'}) ."s";
					}
					error("STATUS: currently running: ". join(', ', @run_status));
				}
				$last_progress = time;
			}
			next;
		}
		# Run next
		run_job(shift(@queue)) if @queue;
	}
	if ($progress) {
		printf STDERR ts() ."--- Queue processed. %d jobs executed, %d skipped, %d killed.\n", $jobs_executed, $jobs_skipped, scalar(@jobs_killed);
	}
}

sub run_perpetually {
	if (-e $lockfile) {
		die "Lockfile exists. Please make sure no other instance of jobd is running.";
	}
	open LOCK, '>', $lockfile || die "Cannot create lockfile $lockfile: $!";
	print LOCK $$;
	close LOCK;
	$locked = 1;

	while ($perpetual) {
		queue_all();
		run_queue();
		sleep($restart_delay) if $perpetual; # Let the system breathe for a moment
	}
	unlink $lockfile;
}

######### Helpers {{{1

sub error($) {
	print STDERR shift()."\n";
}
sub fatal($) {
	error(shift);
	exit 1;
}

######### Main {{{1

# Parse options
Getopt::Long::Configure('bundling');
my $parse_res = GetOptions(
	'help|?' => sub { pod2usage(-verbose => 1, -exitval => 0); },
	'quiet|q' => \$quiet,
	'progress|P' => \$progress,
	'kill-after|k=i' => \$kill_after,
	'max-parallel|p=i' => \$max_par,
	'max-intensive-parallel|i=i' => \$max_par_intensive,
	'lockfile|l=s' => \$lockfile,
	'all-once|a' => \$all_once,
	'one|o=s' => \$one,
) || pod2usage(2);
fatal("Error: can only use one out of --all-once and --one")
	if ($all_once && $one);

unless ($quiet) {
	$ENV{'show_progress'} = '1';
	$progress = 1;
}

if ($one) {
	queue_one($one);
	run_queue();
	exit;
}

if ($all_once) {
	queue_all();
	run_queue();
	exit;
}

run_perpetually();

########## Documentation {{{1

__END__

=head1 NAME

jobd - Perform Girocco maintenance jobs

=head1 SYNOPSIS

jobd [options]

 Options:
   -h | --help                           detailed instructions
   -q | --quiet                          run quietly
   -P | --progress                       show occasional status updates
   -k SECONDS | --kill-after=SECONDS     how long to wait before killing jobs
   -p NUM | --max-parallel=NUM           how many jobs to run at the same time
   -i NUM | --max-intensive-parallel=NUM how many resource-hungry jobs to run
                                         at the same time
   -l FILE | --lockfile=FILE             create a lockfile in the given
                                         location
   -a | --all-once                       process the list only once
   -o PRJNAME | --one=PRJNAME            process only one project

=head1 OPTIONS

=over 8

=item B<--help>

Print the full description of jobd's options.

=item B<--quiet>

Suppress non-error messages, e.g. for use when running this task as a cronjob.

=item B<--progress>

Show information about the current status of the job queue occasionally. This
is automatically enabled if --quiet is not given.

=item B<--kill-after=SECONDS>

Kill supervised jobs after a certain time to avoid hanging the daemon.

=item B<--max-parallel=NUM>

Run no more than that many jobs at the same time.

=item B<--max-intensive-parallel=NUM>

Run no more than that many resource-hungry jobs at the same time. Right now,
this refers to repacking jobs.

=item B<--lockfile=FILE>

For perpetual operation, create a lockfile in that place and clean it up after
finishing/aborting.

=item B<--all-once>

Instead of perpetuously processing all projects over and over again, process
them just once and then exit.

=item B<--one=PRJNAME>

Process only the given project (given as just the project name without C<.git>
suffix) and then exit.

=back

=head1 DESCRIPTION

jobd is Girocco's repositories maintenance servant; it periodically checks all
the repositories and updates mirrored repositories and repacks push-mode
repositories when needed.

=cut
