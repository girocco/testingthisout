#!/bin/bash
#
# jobd - Perform Girocco maintenance jobs
# The old jobd.sh has been replaced by jobd.pl; it now simply runs that.

. @basedir@/shlib.sh

exec "$cfg_basedir"/jobd/jobd.pl "$@"

